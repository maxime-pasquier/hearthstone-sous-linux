#!/bin/bash

if [ $# -lt 1 ] ; then
    echo "[usage] $0 argument"
    echo "    - jouer|nettoyer-processus-jouer"
    echo "    - installer|installer-parametres-optimises"
    echo "    - configurer-wine|acceder-base-registre"
    echo "    - activer-bureau-virtuel|desactiver-bureau-virtuel"
    echo "    - winetricks-enregistrer-commandes-fichier"
    echo "    - nettoyer-processus"
    exit
fi

# initialisation
argument=$1

export WINEARCH=win64
export WINEPREFIX=~/.winehq/hearthstone
export WINEDLLOVERRIDES="mscoree,mshtml="
export XMODIFIERS=""
cheminFichierProgramme="$WINEPREFIX/drive_c/Program Files (x86)"
cheminFichierInstallation="$WINEPREFIX/install.exe"
fichierExecution="Battle.net Launcher.exe"
fichierParametres="$WINEPREFIX/drive_c/users/home/AppData/Local/Blizzard/Hearthstone/options.txt"

# création du préfix
if [ ! -d $WINEPREFIX ]; then
    mkdir -p $WINEPREFIX
fi

# avant le lancement
if [ "$argument" = "nettoyer-processus-jouer" -o "$argument" = "nettoyer-processus" ]; then
    echo "[info] nettoyage des processus"
    wineserver -k

    if [ "$argument" = "nettoyer-processus" ]; then
        echo "[info] arrêt"
        exit
    fi

elif [ "$argument" = "installer" ]; then
    if [ -d "$cheminFichierProgramme/Battle.net" ]; then
        echo "[erreur] l'application existe déjà, supprimez-la avant de l'installer"
        exit
    fi

elif [ "$argument" = "installer-parametres-optimises" ]; then
    printf "musicvolume=0\r\nsoundvolume=0,3\r\ngraphicsquality=0\r\ngraphicsfullscreen=True\r\ngraphicswidth=1440\r\ngraphicsheight=900\r\nScreenshakeEnabled=False\r\nhasseennewcinematic=True\r\ntargetframerate=60\r\nvsync=1\r\n" >> $fichierParametres
    exit
fi

# lancement wine
wineboot

if [ "$argument" = "installer" ]; then
    # polices
    winetricks fonts arial

    # bureau virtuel et plein écran
    winetricks settings grabfullscreen=y
    winetricks settings vd=1440x900

    winetricks dlls vcrun2019
    # n existe plus ? mais plus besoin depuis wine-6.19 #winetricks dlls d9vk040
    # plus besoin depuis wine-6.2 #winetricks dlls dotnet48

    # à la fin
    winetricks settings win10

    wget -O "$cheminFichierInstallation" "https://eu.battle.net/download/getInstaller?os=win&installer=Battle.net-Setup.exe"
    wine "$cheminFichierInstallation"

elif [ "$argument" = "installer-fichier" ]; then
    wine "$cheminFichierInstallation"

elif [ "$argument" = "configurer-wine" ]; then
    winecfg

elif [ "$argument" = "acceder-base-registre" ]; then
    wine regedit

elif [ "$argument" = "winetricks-enregistrer-commandes-fichier" ]; then
    winetricks list-all > winetricks-toutes-les-commandes.txt

elif [ "$argument" = "activer-bureau-virtuel" ]; then
    winetricks settings vd=1440x900

elif [ "$argument" = "desactiver-bureau-virtuel" ]; then
    winetricks settings vd=off

elif [ "$argument" = "jouer" -o "$argument" = "nettoyer-processus-jouer" ]; then
    cd "$cheminFichierProgramme/Battle.net"
    wine "$fichierExecution"

else
    echo "[info] aucune commande valide"

fi
